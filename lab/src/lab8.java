/* Disciplina: Computacao Concorrente */
/* Prof.: Silvana Rossetto */
/* Codigo: Leitores e escritores usando monitores em Java */
/* -------------------------------------------------------------------*/
/* Michel Monteiro Schorr  -  120017379 */
// Monitor que implementa a logica do padrao leitores/escritores



class Numero {
    int num;
}



class LE {
  private int leit, escr;  
  
  // Construtor
  LE() { 
     this.leit = 0; //leitores lendo (0 ou mais)
     this.escr = 0; //escritor escrevendo (0 ou 1)
  } 
  
  // Entrada para leitores
  public synchronized void EntraLeitor (int id) {
    try { 
      while (this.escr > 0) {
      //if (this.escr > 0) {
         System.out.println ("le.leitorBloqueado("+id+")");
         wait();  //bloqueia pela condicao logica da aplicacao 
      }
      this.leit++;  //registra que ha mais um leitor lendo
      System.out.println ("le.leitorLendo("+id+")");
    } catch (InterruptedException e) { }
  }
  
  // Saida para leitores
  public synchronized void SaiLeitor (int id) {
     this.leit--; //registra que um leitor saiu
     if (this.leit == 0) 
           this.notify(); //libera escritor (caso exista escritor bloqueado)
     System.out.println ("le.leitorSaindo("+id+")");
  }
  
  // Entrada para escritores
  public synchronized void EntraEscritor (int id) {
    try { 
      while ((this.leit > 0) || (this.escr > 0)) {
      //if ((this.leit > 0) || (this.escr > 0)) {
         System.out.println ("le.escritorBloqueado("+id+")");
         wait();  //bloqueia pela condicao logica da aplicacao 
      }
      this.escr++; //registra que ha um escritor escrevendo
      System.out.println ("le.escritorEscrevendo("+id+")");
    } catch (InterruptedException e) { }
  }
  
  // Saida para escritores
  public synchronized void SaiEscritor (int id) {
     this.escr--; //registra que o escritor saiu
     notifyAll(); //libera leitores e escritores (caso existam leitores ou escritores bloqueados)
     System.out.println ("le.escritorSaindo("+id+")");
  }
}






//Incrementa valor atual do recurso compartilhado
class T1 extends Thread {
    int id; //identificador da thread
    LE monitor; //objeto monitor para coordenar a lógica de execução das threads
    Numero var; //recurso compartilhado/recurso central

    T1 (int id, LE m, Numero var) {
        this.id = id;
        this.monitor = m;
        this.var = var;
    }


    public void run(){
        try {
            for(;;){
                this.monitor.EntraEscritor(this.id); 
                System.out.println ("print('" + "Thread T1(" + this.id + "): incrementando valor atual " + this.var.num + " em 1')");
                this.var.num++;
                System.out.println ("print('" + "Thread T1(" + this.id + "): valor atual " + this.var.num + "')");
                this.monitor.SaiEscritor(this.id); 
                sleep(500);
            }
        } catch (InterruptedException e) { return; }
    }

    
}



//Printa valor atual do recurso compartilhado
class T2 extends Thread {
    int id; //identificador da thread
    LE monitor; //objeto monitor para coordenar a lógica de execução das threads
    Numero var; //recurso compartilhado/recurso central

    T2 (int id, LE m, Numero var) {
    this.id = id;
    this.monitor = m;
    this.var = var;
    }


    public void run(){
        try {
            for(;;){
                this.monitor.EntraLeitor(this.id); 
                //(this.var.num%2==0? System.out.println ("É par"): System.out.println ("É impar"));
                System.out.println (this.var.num%2==0? "print('" + "Thread T2(" + this.id + "): valor atual " + this.var.num + " é par" + "')": "print('" + "Thread T2(" + this.id + "): valor atual " + this.var.num + " é impar" + "')");
                this.monitor.SaiLeitor(this.id); 
                sleep(500);
            }
        } catch (InterruptedException e) { return; }
    }

    
}



//Printa valor atual do recurso compartilhado depois seta ele para o valor do id da thread
class T3 extends Thread {
    int id, i; //identificador da thread
    LE monitor; //objeto monitor para coordenar a lógica de execução das threads
    Numero var; //recurso compartilhado/recurso central


    double j=77777777777777777777777.7;    //variavel pra processamento "bobo"

    T3 (int id, LE m, Numero var) {
        this.id = id;
        this.monitor = m;
        this.var = var;
    }


    public void run(){
        try {
            for(;;){
                this.monitor.EntraLeitor(this.id); 
                System.out.println ("print('" + "Thread T3(" + this.id + "): valor atual " + this.var.num + "')");
                this.monitor.SaiLeitor(this.id); 

                for (int i=0; i<100000000; i++) {j=j/2;} //processamento bobo para simbolizar o tempo de leitura

                this.monitor.EntraEscritor(this.id); 
                this.var.num = this.id;
                System.out.println ("print('" + "Thread T3(" + this.id + "): valor atual " + this.var.num + " foi mudado para o id')");
                this.monitor.SaiEscritor(this.id); 
                sleep(500);
            }
        } catch (InterruptedException e) { return; }
    }
}








class Lab8 {
  static final int numT1 = 1;
  static final int numT2 = 1;
  static final int numT3 = 1;



  public static void main (String[] args) {

    LE monitor = new LE();          // Monitor (objeto compartilhado entre leitores e escritores)

    Numero var =  new Numero();     //elemento central
    var.num = 0;

    int i;



    T1[] t1 = new T1[numT1];    //T1
    T2[] t2 = new T2[numT2];    //T2
    T3[] t3 = new T3[numT3];    //T3





    //inicia o log de saida
    System.out.println ("import verificaLE");
    System.out.println ("le = verificaLE.LE()");
    




    for (i=0; i<numT1; i++) {
       t1[i] = new T1(i, monitor, var);
       t1[i].start(); 
    }
    for (i=0; i<numT2; i++) {
       t2[i] = new T2(i, monitor, var);
       t2[i].start(); 
    }
    for (i=0; i<numT3; i++) {
       t3[i] = new T3(i, monitor, var);
       t3[i].start(); 
    }
  }
}
